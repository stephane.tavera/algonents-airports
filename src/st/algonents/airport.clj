(ns st.algonents.airport
  (:require
    [clojure.data.json :as json]
    [next.jdbc :as jdbc]
    [honey.sql :as sql]
    [honey.sql.helpers :as h]))

(def db {:dbtype "postgresql"
         :dbname "stephanetavera"})

(defn build-data-source
  []
  (jdbc/get-datasource db))

(def data-source (memoize build-data-source))

(defn http-response
  [status data]
  {:status status
   :body   (json/write-str data)})

(defn fetch-airport
  [airport-id]
  (let [sql-clause (sql/format {:select [:lat :lon], :from [:airports], :where [:= :id airport-id]})
        sql-res (jdbc/execute! (data-source) sql-clause)
        airport (first sql-res)]
    (if (empty? sql-res)
      (http-response 400 {:error (str "Unknown airport '" airport-id "'")})
      (http-response 200 airport))))

(defn update-airport
  [request]
  (let [{:strs [lat lon airport-id]} (:body request)
        sql-clause (-> (h/update :airports)
                       (h/set {:lat lat
                               :lon lon})
                       (h/where [:= :id airport-id])
                       sql/format)
        sql-res (jdbc/execute! (data-source) sql-clause)
        nb-updates (-> sql-res
                       first
                       ::jdbc/update-count)]
    (if (= 0 nb-updates)
      (http-response 400 {:error (str "Update failed")})
      (http-response 200 {:id      airport-id
                          :updated (System/currentTimeMillis)}))))

