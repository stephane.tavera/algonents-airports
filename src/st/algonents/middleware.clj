(ns st.algonents.middleware
  (:require
    [ring.middleware.json :refer [wrap-json-body]]
    [ring.middleware.defaults :refer [site-defaults wrap-defaults]]
    [ring.middleware.multipart-params :refer [wrap-multipart-params]]
    [st.algonents.middleware.logger :refer [wrap-logger]]))

(defn wrap-middleware
  [handler]
  (-> handler
      wrap-json-body
      wrap-multipart-params
      (wrap-defaults (-> site-defaults
                         (assoc-in [:security :anti-forgery] false)))
      wrap-logger))
