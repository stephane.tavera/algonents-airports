(ns st.algonents.server
  (:require
    [ring.adapter.jetty :as jetty]
    [st.algonents.handler :refer [app]])
  (:import (org.eclipse.jetty.server Server)))

(defn start-server
  [port]
  (let [server (jetty/run-jetty app {:port  port
                                     :join? false})]
    (println "Server started on " port)
    server))

(defn stop-server
  [^Server server]
  (.stop server))

(defn -main
  [& options]
  (start-server 5000)
  @(promise))
