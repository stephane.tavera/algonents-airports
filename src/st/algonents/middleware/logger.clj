(ns st.algonents.middleware.logger
  (:require
    [clojure.string :as str]
    [taoensso.timbre :refer [info]]))

(defn- elapsed-time-millis
  [start current-time-millis]
  (str "(" (- current-time-millis start) " ms)"))

(defn- display-space-separated
  [& messages]
  (str/join " " messages))

(defn wrap-logger
  "Logs response with status and elapsed time"
  [handler]
  (fn [request]
    (let [start (System/currentTimeMillis)

          response (handler request)

          status (:status response)
          elapsed (elapsed-time-millis start (System/currentTimeMillis))
          ip (:remote-addr request)
          verb (-> request :request-method name (.toUpperCase))
          uri (:uri request)

          error (if (<= 400 status) (:body response) "")

          _ (info (display-space-separated
                    status
                    elapsed
                    ip
                    verb
                    uri
                    error))]
      response)))

