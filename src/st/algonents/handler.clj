(ns st.algonents.handler
  (:require
    [compojure.core :refer [GET POST defroutes]]
    [st.algonents.airport :as airport]
    [st.algonents.middleware :refer [wrap-middleware]]))

(defroutes routes
           (GET "/airport/:airport-id" [airport-id] (airport/fetch-airport airport-id))
           (POST "/airport" [:as request] (airport/update-airport request)))

(def app (wrap-middleware #'routes))