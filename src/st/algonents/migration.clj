(ns st.algonents.migration
  (:require [clojure.java.io :as io]
            [next.jdbc :as jdbc]
            [tech.v3.dataset :as ds]
            [tech.v3.dataset.sql :as ds-sql]))

(defn initial-dataset
  []
  (-> "a.csv"
      io/resource
      (.getPath)
      (ds/->dataset {:dataset-name "airports"
                     :file-type    :csv
                     :separator    ","})))

(comment

  ;; Connection to PostgreSQL
  (def dev-conn (doto (-> (ds-sql/postgre-sql-connect-str
                            "localhost:5432" "stephanetavera"
                            "stephanetavera" "unsafe")
                          (jdbc/get-connection {:auto-commit false}))
                  (.setCatalog "dev-user")))

  ;; Show what table creation looks like
  (println (ds-sql/create-sql
             dev-conn
             (initial-dataset)
             {}))

  ;; Create the table
  (ds-sql/create-table!
    dev-conn
    (initial-dataset)
    {:primary-key "ID"})

  ;; Insert dataset
  (ds-sql/insert-dataset!
    dev-conn
    (initial-dataset))

  )