(ns st.algonents.airport-test
  (:require [clojure.test :refer [deftest is]])
  (:require [st.algonents.airport :as a]))

;; not really unit tests

(deftest http-get-test
  (is (= {:body   "{\"lat\":46.2383333333333,\"lon\":6.10944444444444}"
          :status 200}
         (a/fetch-airport "LSGG")))
  (is (= {:body   "{\"error\":\"Unknown airport 'X'\"}"
         :status 400}
         (a/fetch-airport "X")))
  )
